package org.esa.must.pds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.RandomAccessFile;
import java.net.URL;
import java.nio.file.Paths;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class DatasetCreatorTest {
	
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Test
	public void testDoesNotExist() {
		
		try {
		    URL dsFileUrl = DatasetCreatorTest.class.getResource("/datasets.yaml");
            File dsFile = new File(dsFileUrl.getFile());
			new DatasetCreator(dsFile,  "NON-EXISTANT", true, true);
			fail("Expected exception");
		} catch (IllegalArgumentException ex) {
			
		}
		
		
	}
	
	
	@Test
	public void testOverwrite() {
	    
	    URL dsFileUrl = DatasetCreatorTest.class.getResource("/datasets.yaml");
        File dsFile = new File(dsFileUrl.getFile());
	    
	    new DatasetCreator(dsFile, folder.getRoot().getAbsolutePath(), true, true);
	    
	    try {
            new DatasetCreator(dsFile, folder.getRoot().getAbsolutePath(), false, true);
            fail("Expected exception");
        } catch (IllegalArgumentException ex) {
            
        }
	    
	    new DatasetCreator(dsFile, folder.getRoot().getAbsolutePath(), true, true);
	}
	
	
	
	
	
	
	@Test
    public void testBinaryValue() {
        
        
        URL dsFileUrl = DatasetCreatorTest.class.getResource("/datasets.yaml");
        File dsFile = new File(dsFileUrl.getFile());
        String root =  folder.getRoot().getAbsolutePath();
        
        DatasetCreator sc = new DatasetCreator(dsFile, root, true, false);
        
        File expectedDat = Paths.get(root, "RO-X-HK-3-AOC_GEN-V1.0", "EXTRAS", "CURRENT_AOCS_MODE", "2004", "ROS_HK_NACP1029_2004.DAT").toFile();
        
        assertTrue(expectedDat.exists());

        
        
        
        // Check  the values in the import.sql file of the test resource folder
        checkTableContent(expectedDat, 2, new Double(1078226716644L), new Float(11.25));
        checkTableContent(expectedDat, 3, new Double(1078226717644L), new Float(211.0));
        checkTableContent(expectedDat, 0, new Double(1078226714644L), new Float(11.0));
        
        
        expectedDat = Paths.get(root, "RO-X-HK-3-AOC_GEN-V1.0", "DATA", "CURRENT_AOCS_MODE", "2004", "ROS_HK_NACP1029_2004.TAB").toFile();
        
        assertTrue(expectedDat.exists());
      
        
        
        expectedDat = Paths.get(root, "RO-X-HK-3-AOC_GEN-V1.0", "BROWSE", "CURRENT_AOCS_MODE_2004_Q1.LBL").toFile();       
        assertTrue(expectedDat.exists());
        
    }
	
	@Test
    public void testMonthlyBrowse() {
        
        
        URL dsFileUrl = DatasetCreatorTest.class.getResource("/monthly_browse.yaml");
        File dsFile = new File(dsFileUrl.getFile());
        String root =  folder.getRoot().getAbsolutePath();
       
        DatasetCreator sc = new DatasetCreator(dsFile, root, true, false);
        
        File expectedDat = Paths.get(root, "RO-X-HK-3-AOC_GEN-V1.0", "BROWSE", "CURRENT_AOCS_MODE_2004_03.LBL").toFile();
        
        assertTrue(expectedDat.exists());
        
    }
	
	private void checkTableContent (File file, int index, Double expectedTime, Float expectedValue) 
    {
          final int RECORD_SIZE = 12;   // 12 bytes
          long byteNum;              // For the byte number

          try {
              
          // Open the file for reading.
          RandomAccessFile randomFile =
                     new RandomAccessFile(file, "r");
          
          // Move to the record
          // from the beginning of the file.
          byteNum = RECORD_SIZE * index;
          randomFile.seek(byteNum);
          
          // Read the timestamp and value
          double time = randomFile.readDouble();
          float value = randomFile.readFloat();
	    
          randomFile.close();
          
          assertEquals("Value componet is not the same", expectedValue, value, 0.0001);
          assertEquals("Time componet is not the same", expectedTime, time, 0.0001);
          
          } catch (Exception e) {
              fail("Unexpected exception");
          }
	    
	     
	}

}
