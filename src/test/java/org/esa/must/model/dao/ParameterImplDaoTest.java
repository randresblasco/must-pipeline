package org.esa.must.model.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.esa.must.model.Parameter;
import org.esa.must.model.ParameterValue;
import org.esa.must.model.dao.impl.ParameterDaoImpl;
import org.junit.Test;

public class ParameterImplDaoTest {
    
    ParameterDao dao = new ParameterDaoImpl();

    @Test
    public void testGetValues() {
        
        try {
            Parameter parameter = dao.findByName("NACP1029");
            assertNotNull(parameter);
            List<ParameterValue> values = dao.getValues(parameter, 0L, 0L);
            assertEquals(0, values.size());
            values = dao.getValues(parameter, 1078226714643L, 1078226719645L);
            assertEquals(5, values.size());
        } catch (DaoException e) {
            fail("Unexpected exception");
        }
        
        
    }

    @Test
    public void testFindByName() {
        
        try {
            Parameter findByName = dao.findByName("NON_EXISTANT");
            assertNull(findByName);
        } catch (DaoException e) {
            fail("Unexpected exception");
        }
        
        try {
            Parameter findByName = dao.findByName("NACP1029");
            assertNotNull(findByName);
        } catch (DaoException e) {
            fail("Unexpected exception");
        }
       
    }

    @Test
    public void testFindById() {
        try {
            Parameter findByName = dao.findById(Parameter.class, -1);
            assertNull(findByName);
        } catch (DaoException e) {
            fail("Unexpected exception");
        }
        try {
            Parameter findByName = dao.findById(Parameter.class, 85);
            assertNotNull(findByName);
        } catch (DaoException e) {
            fail("Unexpected exception");
        }
    }

    @Test
    public void testFindAll() {
        try {
            List<Parameter> parameters = dao.findAll(Parameter.class);
            assertNotNull(parameters);
            assertEquals(3, parameters.size());
        } catch (DaoException e) {
            fail("Unexpected exception");
        }
    }

}
