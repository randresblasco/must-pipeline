INSERT INTO parameter (PNAME, PID, PDESCR, SSNAME, UNITS, DBTYPE, PTYPEID) VALUES ('NACP1029', 85, 'STR A Absolute Frame Nb', 'SACSTRSW', '.', 'float', 1);
INSERT INTO parameter (PNAME, PID, PDESCR, SSNAME, UNITS, DBTYPE, PTYPEID) VALUES ('NACP0000', 1, 'TC Packet ID', 'SACSSW00', null, 'uSmallInt', 1);
INSERT INTO parameter (PNAME, PID, PDESCR, SSNAME, UNITS, DBTYPE, PTYPEID) VALUES ('WRONGDEF', 99, 'Wrong definition', 'WRONGDEF', null, 'WRONGTABLE', 1);


CREATE TABLE floatparamvalues (PID smallint(9) unsigned DEFAULT 0 NOT NULL, datetime decimal(13,0) unsigned DEFAULT 0 NOT NULL, value float DEFAULT 0 NOT NULL, PRIMARY KEY (PID, datetime));


INSERT INTO floatparamvalues (PID, datetime, value) VALUES (85, 1078226714644, 11.0);
INSERT INTO floatparamvalues (PID, datetime, value) VALUES (85, 1078226715644, 11.1);
INSERT INTO floatparamvalues (PID, datetime, value) VALUES (85, 1078226716644, 11.25);
INSERT INTO floatparamvalues (PID, datetime, value) VALUES (85, 1078226717644, 211.0);
INSERT INTO floatparamvalues (PID, datetime, value) VALUES (85, 1078226719644, 211.0);


CREATE TABLE usmallintparamvalues (PID smallint(9) unsigned DEFAULT 0 NOT NULL, datetime decimal(13,0) unsigned DEFAULT 0 NOT NULL, value smallint(5) unsigned DEFAULT 0 NOT NULL, PRIMARY KEY (PID, datetime));

INSERT INTO usmallintparamvalues (PID, datetime, value) VALUES (1, 1078226714644, 11);
INSERT INTO usmallintparamvalues (PID, datetime, value) VALUES (1, 1078226715644, 12);
INSERT INTO usmallintparamvalues (PID, datetime, value) VALUES (1, 1078226716644, 11);
INSERT INTO usmallintparamvalues (PID, datetime, value) VALUES (1, 1078226717644, 1444);
INSERT INTO usmallintparamvalues (PID, datetime, value) VALUES (1, 1078226718644, 1445);