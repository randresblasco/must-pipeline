package org.esa.must.model;

public class ParameterValue {

    private Double timestamp;
    private Object value;
    
    
    
    public ParameterValue(Double timestamp, Object value) {
        super();
        this.timestamp = timestamp;
        this.value = value;
    }
    
    public Double getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }
    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }
    
    
}
