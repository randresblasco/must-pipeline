package org.esa.must.model.dao.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.esa.must.model.dao.DaoException;
import org.esa.must.model.dao.GenericDao;
import org.hibernate.Session;
import org.hibernate.query.Query;



public class GenericDaoImpl<T> implements GenericDao<T> {

    @SuppressWarnings("unused")
    private static Logger LOGGER = Logger.getLogger(GenericDaoImpl.class.getName());

    

    
    @Override
    public T findById(Class<T> clazz, Integer id) throws DaoException {
        // This method is used by the Request Factory Locators
        HibernateUtil.beginTransaction();
        Session session = HibernateUtil.getSession();
        
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(clazz);
        Root<T> root = query.from(clazz);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<T> q=session.createQuery(query);
        T t = (T) q.uniqueResult();
        
        HibernateUtil.commitTransaction();
        return t;
    }
    
    
    
    

    @Override
    public List<T> findAll(Class<T> clazz) throws DaoException {
        try {
            HibernateUtil.beginTransaction();
            Session session = HibernateUtil.getSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<T> query = builder.createQuery(clazz);
            Root<T> root = query.from(clazz);
            query.select(root);
            Query<T> q=session.createQuery(query);
            List<T> items = q.list();
            HibernateUtil.commitTransaction();
            return items;
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            HibernateUtil.closeSession();
        }
    }

}