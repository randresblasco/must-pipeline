/*
 * This file is part of the BepiColombo Science Ground Segment software.
 *
 * Copyright (c) 2015- European Space Agency
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.esa.must.model.dao;

import java.util.List;

public interface GenericDao<T> {

	public T findById(Class<T> clazz, Integer id) throws DaoException;

	public List<T> findAll(Class<T> clazz) throws DaoException;

}