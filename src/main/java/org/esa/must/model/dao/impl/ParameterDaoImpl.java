package org.esa.must.model.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.esa.must.model.DBType;
import org.esa.must.model.Parameter;
import org.esa.must.model.ParameterValue;
import org.esa.must.model.dao.DaoException;
import org.esa.must.model.dao.ParameterDao;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.type.DoubleType;


public class ParameterDaoImpl extends GenericDaoImpl<Parameter> implements ParameterDao  {

    @Override
    public List<ParameterValue> getValues(Parameter parameter, Long st, Long et) throws DaoException {
        
        List<ParameterValue> items = new ArrayList<>();
        
        DBType dbType = DBType.getDBTypeFromName(parameter.getDbType());
        
        String queryStr = "select datetime, value from " + dbType.getTablename() + " where pid = :pid and datetime > :st and datetime < :et";
        
        try {
            HibernateUtil.beginTransaction();
            Session session = HibernateUtil.getSession();
           
            NativeQuery query = session.createSQLQuery(queryStr)
                    .addScalar( "datetime", DoubleType.INSTANCE )
                    .addScalar( "value", dbType.getValueType())
                    .setParameter("pid", parameter.getId())
                    .setParameter("st", st)
                    .setParameter("et", et);
           
            List<Object[]> results = query.list();
            for(Object[] result : results) {
                Double timestamp = (Double) result[0];
                items.add(new ParameterValue(timestamp, result[1]));
            }
            
            
            HibernateUtil.commitTransaction();
            return items;
        } catch (Exception e) {
            
            throw new DaoException(e);
        } finally {
            //HibernateUtil.closeSession();
        }
        
    }



    @Override
    public Parameter findByName(String pname) throws DaoException {
        try {
            HibernateUtil.beginTransaction();
            Session session = HibernateUtil.getSession();
            
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Parameter> query = builder.createQuery(Parameter.class);
            Root<Parameter> root = query.from(Parameter.class);
            query.select(root).where(builder.equal(root.get("pname"), pname));
            Query<Parameter> q=session.createQuery(query);
            Parameter parameter = q.uniqueResult();
            HibernateUtil.commitTransaction();

            return parameter;
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            //HibernateUtil.closeSession();
        }
    }


   

}
