package org.esa.must.model.dao.impl;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.esa.must.model.Entities;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;



public class HibernateUtil {

    private static final Logger LOGGER = Logger.getLogger(HibernateUtil.class.getName());

    private static final SessionFactory sessionFactory = buildSessionFactory();

    public static SessionFactory buildSessionFactory() {
        try {

            Configuration configuration = new Configuration();

            Properties databaseProperties = new Properties();
            databaseProperties.load(HibernateUtil.class.getClassLoader().getResourceAsStream("database.properties"));
            configuration.setProperty("hibernate.connection.url", databaseProperties.getProperty("database.url"));
            configuration.setProperty("hibernate.connection.username",
                    databaseProperties.getProperty("database.username"));
            configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
            configuration.setProperty("hibernate.connection.driver_class",
                    databaseProperties.getProperty("database.driverClassName"));
            configuration.setProperty("hibernate.connection.password",
                    databaseProperties.getProperty("database.password"));

            configuration.setProperty("hibernate.default_schema",
                    databaseProperties.getProperty("database.schema"));

            // sql properties - helps reducing SQL printed to stdout
            configuration.setProperty("hibernate.show_sql", "false");
            configuration.setProperty("hibernate.format_sql", "false");
            configuration.setProperty("hibernate.use_sql_comments", "false");

            // other properties
            configuration.setProperty("hibernate.current_session_context_class", "thread");
            configuration.setProperty("hibernate.bytecode.use_reflection_optimizer", "false");
            configuration.setProperty("javax.persistence.validation.mode", "none");

            // ONLY for testing purposes
            if (databaseProperties.getProperty("hibernate.hbm2ddl.auto") != null) {
                configuration.setProperty(Environment.HBM2DDL_AUTO,
                        databaseProperties.getProperty("hibernate.hbm2ddl.auto"));
            }

            // ADD ANNOTATED CLASSES

            for (Class<?> entityClass : Entities.getAllEntitiyClasses()) {
                configuration.addAnnotatedClass(entityClass);
            }

            LOGGER.info("Initializing session factory...");
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
            SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            LOGGER.info("Session factory initialized successfully.");
            return sessionFactory;

        } catch (Exception e) {
            LOGGER.error("Error initializing Hibernate's session factory");
            e.printStackTrace();
            throw new ExceptionInInitializerError(e);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        getSessionFactory().close();
    }

    public static Session beginTransaction() {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        return session;
    }

    public static void commitTransaction() {
        HibernateUtil.getSession().getTransaction().commit();
    }

    public static void rollbackTransaction() {
        HibernateUtil.getSession().getTransaction().rollback();
    }

    public static void closeSession() {
        HibernateUtil.getSession().close();
    }

    public static void flushSession() {
        HibernateUtil.getSession().flush();
    }

    public static void clearSession() {
        HibernateUtil.getSession().clear();
    }

    public static Session getSession() {
        Session session = sessionFactory.getCurrentSession();
        return session;
    }

}