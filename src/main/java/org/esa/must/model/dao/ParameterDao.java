package org.esa.must.model.dao;

import java.util.List;

import org.esa.must.model.Parameter;
import org.esa.must.model.ParameterValue;


public interface ParameterDao extends GenericDao<Parameter> {
    
    Parameter findByName(String pname) throws DaoException;

    List<ParameterValue> getValues(Parameter parameter, Long startTime, Long endTime)  throws DaoException;


}