package org.esa.must.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;

import org.hibernate.type.LongType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.StringType;
import org.hibernate.type.FloatType;



public enum DBType {
    
   
    
    bitparam(LongType.INSTANCE, "ASCII_INTEGER", "MSB_INTEGER", "I5", 15, 15, true),
    doubleparam(DoubleType.INSTANCE, "ASCII_REAL", "IEEE_REAL", "E20.6", 20, 8, false),
    floatparam(FloatType.INSTANCE, "ASCII_REAL", "IEEE_REAL", "E20.6",20, 4, false),
    sintparam(LongType.INSTANCE, "ASCII_INTEGER", "MSB_INTEGER", "I5", 15, 15, true),
    smediumintparam(LongType.INSTANCE, "ASCII_INTEGER", "MSB_INTEGER", "I5", 15, 15, true),
    ssmallintparam(LongType.INSTANCE, "ASCII_INTEGER", "MSB_INTEGER", "I5", 15, 15, true),
    stinyintparam(LongType.INSTANCE, "ASCII_INTEGER", "MSB_INTEGER", "I5", 15, 15, true),
    stringparam(StringType.INSTANCE, "CHARACTER", "CHARACTER", "A65535", 65535, 65535, true),
    uintparam(LongType.INSTANCE, "ASCII_INTEGER", "MSB_INTEGER", "I15", 15, 15, true),
    umediumintparam(LongType.INSTANCE, "ASCII_INTEGER", "MSB_INTEGER", "I5", 15, 15, true),
    usmallintparam(LongType.INSTANCE, "ASCII_INTEGER", "MSB_INTEGER", "I5", 15, 15, true),
    utinyintparam(IntegerType.INSTANCE, "ASCII_INTEGER", "MSB_INTEGER", "I5", 15, 15,true);
    
    private AbstractSingleColumnStandardBasicType type;
    private String asciiDataType;
    private String dataTypeFormat;
    private int binDataTypeBytes;
    private int asciiDataTypeBytes;
    private boolean isAscii;
    private String binDataType;
    
    static DecimalFormat  FLOAT_FORMATTER = new DecimalFormat("#########.######E0");

    public static DBType getDBTypeFromName(String name) {
        return DBType.valueOf(name.toLowerCase() + "param");
    }

    private DBType(AbstractSingleColumnStandardBasicType type, String asciiDataType, String binDataType, String dataTypeFormat,
            int asciiDataTypeBytes, int binDataTypeBytes, boolean isAscii) {
        this.type = type;
        this.asciiDataType = asciiDataType;
        this.binDataType = binDataType;
        this.dataTypeFormat = dataTypeFormat;
        this.binDataTypeBytes = binDataTypeBytes;
        this.asciiDataTypeBytes = asciiDataTypeBytes;
        this.isAscii = isAscii;
    }

    public String formatValue(Object value) {
        
        if (isAscii) {
            return value.toString();
        } else {
          
            return String.format("%20.6E", value);
        }
        
    }
    
    
    public String getTablename() {
        return name() + "values";
    }
    
    public AbstractSingleColumnStandardBasicType getValueType() {
        return this.type;
    }

    public int getAsciiDataTypeBytes() {
        return this.asciiDataTypeBytes;
    }
    
    public String getBinDataType() {
        return binDataType;
    }
    public int getBinDataTypeBytes() {
        return binDataTypeBytes;
    }

    public String getAsciiDataType() {
        return this.asciiDataType;
    }

    public String getDataTypeFormat() {
        return this.dataTypeFormat;
    }

    public int getBinMaxRowBytes() {
        int timeField = 8;
        int dataTypeBytes = getBinDataTypeBytes();
        return timeField + dataTypeBytes;
    }
    
    
    public int getAsciiMaxRowBytes () {
        int separator = 1;
        int eol = 2;
        int dataTypeBytes = getAsciiDataTypeBytes();
        int asciiTimefield = 23; 
        
        return asciiTimefield + separator + dataTypeBytes + eol;
        
    }

    public boolean isAscii() {
        return this.isAscii;
    }

    
    public static void main(String[] args) {
       
        double value = 0.1234567890123456789;
        value = 0;
        
        String valueStr = FLOAT_FORMATTER.format(value);
        
        System.out.println(valueStr + String.format("(%d)", valueStr.length()));
        System.out.println(String.format("%20.7E", value));

        
    }
}






