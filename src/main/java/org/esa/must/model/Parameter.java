package org.esa.must.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "parameter")
public class Parameter implements java.io.Serializable {

   
    private static final long serialVersionUID = 1828332814609586811L;
    private String pname;
    private Integer id;
    private String pDescr;
    private String ssName;
    private String units;
    private String dbType;
    private Integer pTypeId;

    @Column(name = "PNAME")
    public String getPname() {
        return pname;
    }
    @Id
    @Column(name = "PID")
    public Integer getId() {
        return id;
    }
    
    public void setPname(String pname) {
        this.pname = pname;
    }
    public void setpDescr(String pDescr) {
        this.pDescr = pDescr;
    }
    public void setSsName(String ssName) {
        this.ssName = ssName;
    }
    public void setUnits(String units) {
        this.units = units;
    }
    public void setDbType(String dbType) {
        this.dbType = dbType;
    }
    public void setpTypeId(Integer pTypeId) {
        this.pTypeId = pTypeId;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "PDESCR")
    public String getpDescr() {
        return pDescr;
    }

    @Column(name = "SSNAME")
    public String getSsName() {
        return ssName;
    }

    @Column(name = "UNITS")
    public String getUnits() {
        return units;
    }

    @Column(name = "DBTYPE")
    public String getDbType() {
        return dbType;
    }

    @Column(name = "PTYPEID")
    public Integer getpTypeId() {
        return pTypeId;
    }
}
