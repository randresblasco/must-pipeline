package org.esa.must.cli;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.log4j.Logger;
import org.esa.must.model.dao.impl.HibernateUtil;
import org.esa.must.pds.Configuration;
import org.esa.must.pds.DatasetCreator;


public class MustPipeline {

    private static Logger LOGGER = Logger.getLogger(MustPipeline.class.getName());
    
    
    public static void main(String[] args) {
        try {
            
            LOGGER.info("MUST-pipeline Version: " + Configuration.getInstance().getSoftwareVersion());
            LOGGER.info("Build Timestamp: " + Configuration.getInstance().getBuildTimestamp());
            
            CommandLineParser parser = new DefaultParser();
            CommandLine commandLine = parser.parse( getOptions(), args);
 
            File rootFolder = getMandatoryFile(commandLine, "r");
            
            File datasets = getMandatoryFile(commandLine, "i");
            
            
            boolean overwrite = false;
            
            if (commandLine.hasOption("o")) {
                overwrite = true;
            }

            boolean filterCometPhase = true;
            if (commandLine.hasOption("a")) {
                filterCometPhase = false;
            }
            
            DatasetCreator sc = new DatasetCreator(datasets.getAbsoluteFile(), rootFolder.getAbsolutePath(), overwrite, filterCometPhase);
            
                 
        } catch (Exception e) {
            LOGGER.error(e);
            e.printStackTrace();
            printHelp();
        } finally {
            HibernateUtil.shutdown();
        }
    }

    private static File getMandatoryFile(CommandLine commandLine, String option) {
       
        File result = new File(commandLine.getOptionValue(option));
        if (!result.exists()) {
            LOGGER.error("The mandatory file for the option " + option + " does not exist");
            throw new IllegalArgumentException("The file " + result.getPath() + " does not exist");
        }
        return result;
    }

    
    private static Options getOptions() {
        
        Options options = new Options();
            
        options.addOption( Option.builder("r").
                            longOpt("root").
                            hasArg().
                            required(true).
                            desc("Root folder (shall exist)").build());
        
        options.addOption( Option.builder("i").
                longOpt("input").
                hasArg().
                required(true).
                desc("Datasets configuration file").build());
        
        options.addOption( Option.builder("o").
                longOpt("overwrite").
                required(false).
                desc("[Optional] Overwrite dataset contents").build());
        
        options.addOption( Option.builder("a").
                longOpt("all_mission").
                required(false).
                desc("[Optional] Generate browse label for all mission").build());
        
        
         return options;
    }

    private static void printHelp() {
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.setNewLine("\n");
        helpFormatter.printHelp("must-pipeline.sh", getOptions());
    }
}
