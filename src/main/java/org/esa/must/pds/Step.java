package org.esa.must.pds;

public enum Step {

    MONTH,
    QUARTER,
    YEAR;
    
}
