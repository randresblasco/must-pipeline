package org.esa.must.pds;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.esa.must.model.DBType;
import org.esa.must.model.Parameter;
import org.esa.must.model.ParameterValue;
import org.esa.must.model.dao.ParameterDao;
import org.esa.must.model.dao.impl.ParameterDaoImpl;
import es.randres.spice.PositionOracle;

public class DatasetCreator {

    private static final int LBL_LINE_CHARACTERS = 78;

    private static final int SPACECRAFT_ID = -226;

    private static Logger LOGGER = Logger.getLogger(DatasetCreator.class.getName());

    private static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private static DateTimeFormatter CREATION_DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
    private static DateTimeFormatter CREATION_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static DateTimeFormatter MONTH_FORMATTER = DateTimeFormatter.ofPattern("yyyy_MM");
    private static DateTimeFormatter QUARTER_FORMATTER = DateTimeFormatter.ofPattern("yyyy_QQQ");
    private static DateTimeFormatter YEAR_FORMATTER = DateTimeFormatter.ofPattern("yyyy");
    
    private static Long COMET_PHASE = 1388534400000L; // 2014-01
    private static Long START_MISSION = 1078095600000L;
    private static Long END_MISSION = 1475272800000L;

    private File root;
    private File dataFolder;
    private File browseFolder;
    private File extrasFolder;
    
    private Replacer replacer;
    private ParameterDao dao;

    private boolean filterCometPhase;

    private PositionOracle spice;

   

    public DatasetCreator(File datasetsFile, String path, boolean overwrite, boolean filterCometPhase) {
        super();

        YamlConfiguration datasets = YamlConfiguration.load(datasetsFile);
        dao = new ParameterDaoImpl();
        spice = new PositionOracle();
        try {
            spice.loadKernels(Arrays.asList(getResourceFile("/spice/NAIF0011.TLS"),
                                            getResourceFile("/spice/ROS_160929_STEP.TSC")
                                            ));
        } catch (Exception e) {
            throw new IllegalArgumentException("SPICE cannot be setup");
        }
        
        this.filterCometPhase = filterCometPhase;

        File parent = new File(path);
        if (!parent.isDirectory() || !parent.exists()) {
            throw new IllegalArgumentException("The parent folder shall exist");
        }

        for (Dataset ds : datasets.getDatasets()) {

            this.root = new File(parent, ds.getId());
            if (root.exists() && !overwrite) {
                throw new IllegalArgumentException("The target folder exists (non-overwrite mode)");
            }
            this.root.mkdir();

            this.dataFolder = new File(this.root, "DATA");
            this.dataFolder.mkdir();

            this.browseFolder = new File(this.root, "BROWSE");
            this.browseFolder.mkdir();

            this.extrasFolder = new File(this.root, "EXTRAS");
            this.extrasFolder.mkdir();
            
            this.replacer = new Replacer();
            replacer.addReplacement("dataset_id", ds.getId());
            replacer.addReplacement("dataset_name", ds.getName());
            replacer.addReplacement("sc_hk_subsystem", ds.getHkSubsystem());


            ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
            replacer.addReplacement("creation_time", now.format(CREATION_DATETIME_FORMATTER));
            replacer.addReplacement("creation_date", now.format(CREATION_DATE_FORMATTER));

            createContent(ds.getProducts());

        }

    }

    public void createContent(List<ProductDefinition> definitions) {

        try {
            Map<String, List<String>> browseLabels = new HashMap<>();
            
            for (ProductDefinition definition : definitions) {

                Parameter parameter = dao.findByName(definition.getParameterId());
                if (parameter != null) {
                    DBType dbType = DBType.getDBTypeFromName(parameter.getDbType());
                    LOGGER.info("Generating product for the parameter " + definition.getParameterId());
                    if (dbType.isAscii()) {
                        generateAsciiParameterProduct(definition, parameter, dbType,
                                Step.valueOf(definition.getFileDuration()), browseLabels);
                    } else {
                        generateAsciiParameterProduct(definition, parameter, dbType,
                                Step.valueOf(definition.getFileDuration()), browseLabels);
                        
                        generateBinaryParameterProduct(definition, parameter, dbType,
                                Step.valueOf(definition.getFileDuration()), browseLabels);
                    }
                } else {
                    LOGGER.warn("The parameter " + definition.getParameterId() + " does not exist");
                }
            }
            
            for (String browseProductId : browseLabels.keySet()) {
              
                this.replacer.addReplacement("plot_id", browseProductId);
                this.replacer.addListReplacement("source_Ids",browseLabels.get(browseProductId));
                generateFromTemplate(this.replacer, this.browseFolder, "/templates/BROWSE", "BROWSE_PNG.LBL", browseProductId + ".LBL");
            }

        } catch (Exception e) {
            LOGGER.error("Error processing parameter", e);
            throw new RuntimeException(e.getMessage());
        }
    }

    private void generateBinaryParameterProduct(ProductDefinition definition, Parameter parameter, DBType dbType,
            Step step, Map<String, List<String>> browseLabels) throws Exception {
        String ssName = definition.getName();
        File ssFolder = new File(extrasFolder, ssName);
        ssFolder.mkdir();

    
       

        Long current = START_MISSION;

        while (current < END_MISSION) {

            String timePeriodFolderSufix = getTimePeriodSufix(current, step); 
            String productId = String.format("ROS_HK_%s_%s", parameter.getPname(), timePeriodFolderSufix);
            String productDec = String.format("This table gives the time profile of the %s parameter ",
                    parameter.getpDescr());
            replacer.addReplacement("product_id", productId);
            replacer.addReplacement("product_desc", productDec);
            replacer.addReplacement("subsystem_data_type", ssName);

            Long endCurrent = current + getStepDuration(step, current);
            List<ParameterValue> values = dao.getValues(parameter, current, endCurrent);
           

            if (!values.isEmpty()) {
                Long totalSize = 0L;
                
                String timePeriodFolderName =  getTimePeriodFolderName(current, step);
                File timePeriodFolder = new File(ssFolder, timePeriodFolderName);
                timePeriodFolder.mkdir();

                File productBinary = new File(timePeriodFolder, productId + ".DAT");

                FileOutputStream fw1 = new FileOutputStream(productBinary);
                BufferedOutputStream bos = new BufferedOutputStream(fw1);
                DataOutputStream dos = new DataOutputStream(bos);

                totalSize += values.size();

                Long startTime = Long.MAX_VALUE;
                Long endTime = Long.MIN_VALUE;

                for (ParameterValue value : values) {

                    Double ts = value.getTimestamp();
                    Long tsLong = ts.longValue();

                    startTime = Math.min(startTime, tsLong);
                    endTime = Math.max(endTime, tsLong);

                    dos.writeDouble(ts);
                    dos.writeFloat(((Float) value.getValue()));
                }

                dos.flush();
                dos.close();

                LOGGER.info("  Binary product " + productId + ".DAT generated (" + totalSize + " values)");

                replacer.addReplacement("file_records", Long.toString(totalSize));
                
                replacer.addReplacement("start_time", formatUTCTime(startTime.doubleValue()));
                replacer.addReplacement("end_time", formatUTCTime(endTime.doubleValue()));
                replacer.addReplacement("start_sclk", spice.date2sclks(SPACECRAFT_ID, getDate(startTime.doubleValue())));
                replacer.addReplacement("end_sclk", spice.date2sclks(SPACECRAFT_ID, getDate(endTime.doubleValue()))); 
                replacer.addReplacement("data_type_bytes", Integer.toString(dbType.getBinDataTypeBytes()));
                replacer.addReplacement("data_type", dbType.getBinDataType());
                replacer.addReplacement("data_type_format", dbType.getDataTypeFormat());
                replacer.addReplacement("units", getUnits(parameter.getUnits()));
                replacer.addReplacement("row_bytes", Integer.toString(dbType.getBinMaxRowBytes()));
  
                
                generateFromTemplate(replacer, timePeriodFolder, "/templates/DATA", "PARAMETER_BINARY.LBL", productId + ".LBL");

                LOGGER.info("  Label generated " + productId + ".LBL");
                
                if(definition.hasThumbnail() && filterCometPhase(current, endCurrent)) {
                    String key = definition.getName() + "_" + timePeriodFolderSufix;
                    if (!browseLabels.containsKey(key)) {
                        browseLabels.put(key, new ArrayList<>());     
                    }
                    browseLabels.get(key).add(productId);
                }
            }

            current += getStepDuration(step, current);
        }

    }

    private Long getStepDuration(Step step, Long current) {
        Long duration = null;

        switch (step) {
        case MONTH:

            duration = getNextMonthStep(current);
            break;

        case QUARTER:
            duration = getNextQuarterStep(current);
            break;

        case YEAR:
            duration = getNextYearStep(current);
            break;

        default:
            duration = null;
            break;
        }

        return duration;
    }

    private String getTimePeriodSufix(Long current, Step step) {
        String sufix = "NA";

        switch (step) {
        case MONTH:
            sufix = formatMonth(current);
            break;

        case QUARTER:
            sufix = formatQuarter(current);
            break;

        case YEAR:
            sufix = formatYear(current);
            break;

        default:
            sufix = "ERROR";
            break;
        }

        return sufix;
    }

    private String getTimePeriodFolderName(Long current, Step step) {

        String folderName = "NA";

        switch (step) {
        case MONTH:
            folderName = formatQuarter(current);
            break;

        case QUARTER:
            folderName = formatQuarter(current);
            break;

        case YEAR:
            folderName = formatYear(current);
            break;

        default:
            folderName = "ERROR";
            break;
        }

        return folderName;
    }

    private void generateAsciiParameterProduct(ProductDefinition definition, Parameter parameter, DBType dbType, Step step, Map<String, List<String>> browseLabels) throws Exception {

        
        String ssName = definition.getName();
        File ssFolder = new File(dataFolder, ssName);
        ssFolder.mkdir();

       
       

        Long current = START_MISSION;

        while (current < END_MISSION) {

            String timePeriodFolderSufix = getTimePeriodSufix(current, step); 
            String productId = String.format("ROS_HK_%s_%s", parameter.getPname(), timePeriodFolderSufix);
            String productDec = String.format("This table gives the time profile of the %s parameter ",
                    parameter.getpDescr());
            replacer.addReplacement("product_id", productId);
            replacer.addReplacement("product_desc", productDec);
            replacer.addReplacement("subsystem_data_type", ssName);

            Long endCurrent = current + getStepDuration(step, current);
            List<ParameterValue> values = dao.getValues(parameter, current, endCurrent);

            if (!values.isEmpty()) {
                
                Long totalSize = 0L;
                
                String timePeriodFolderName =  getTimePeriodFolderName(current, step);
                File timePeriodFolder = new File(ssFolder, timePeriodFolderName);
                timePeriodFolder.mkdir();

        
                File productSpreadsheet = new File(timePeriodFolder, productId + ".TAB");
                BufferedWriter writer = Files.newBufferedWriter(productSpreadsheet.toPath());
                PrintWriter fixedWidthWriter = new PrintWriter(writer);
                

                totalSize += values.size();

                Long startTime = Long.MAX_VALUE;
                Long endTime = Long.MIN_VALUE;

                for (ParameterValue value : values) {

                    Long tsLong = value.getTimestamp().longValue();

                    startTime = Math.min(startTime, tsLong);
                    endTime = Math.max(endTime, tsLong);

                    String utcDate = formatUTCTime(value.getTimestamp());
                    fixedWidthWriter.printf("%s,%" + dbType.getAsciiDataTypeBytes() + "s\r\n", utcDate, dbType.formatValue(value.getValue()));
                }

                fixedWidthWriter.flush();
                fixedWidthWriter.close();

                LOGGER.info("  Ascii product " + productId + ".TAB generated (" + totalSize + " values)");

                replacer.addReplacement("file_records", Long.toString(totalSize));
                replacer.addReplacement("start_time", formatUTCTime(startTime.doubleValue()));
                replacer.addReplacement("end_time", formatUTCTime(endTime.doubleValue()));
                replacer.addReplacement("start_sclk", spice.date2sclks(SPACECRAFT_ID, getDate(startTime.doubleValue())));
                replacer.addReplacement("end_sclk", spice.date2sclks(SPACECRAFT_ID, getDate(endTime.doubleValue()))); 
                replacer.addReplacement("data_type_bytes", Integer.toString(dbType.getAsciiDataTypeBytes()));
                replacer.addReplacement("data_type", dbType.getAsciiDataType());
                replacer.addReplacement("data_type_format", dbType.getDataTypeFormat());
                replacer.addReplacement("units", getUnits(parameter.getUnits()));
                replacer.addReplacement("row_bytes", Integer.toString(dbType.getAsciiMaxRowBytes()));
                generateFromTemplate(replacer, timePeriodFolder, "/templates/DATA", "PARAMETER_ASCII.LBL", productId + ".LBL");

                LOGGER.info("  Label generated " + productId + ".LBL");

                if(definition.hasThumbnail() && filterCometPhase(current, endCurrent)) {
                    String key = definition.getName() + "_" + timePeriodFolderSufix;
                    if (!browseLabels.containsKey(key)) {
                        browseLabels.put(key, new ArrayList<>());     
                    }
                    browseLabels.get(key).add(productId);
                }
            }

            current += getStepDuration(step, current);
        }
        
    }

    private boolean filterCometPhase(Long current, Long endCurrent) {
       
        return !filterCometPhase || current > COMET_PHASE || COMET_PHASE < endCurrent;
    }

    private String getUnits(String units) {
        if (units == null || units.equals("null") || units.equals(".")) {
            return "N/A";
        }
        return units;
    }


    private static Date getDate(Double timestamp) {
        ZonedDateTime zonedDate = Instant.ofEpochMilli(timestamp.longValue()).atZone(ZoneOffset.UTC);
        return Date.from(zonedDate.toInstant());
    }
    
    private static String formatUTCTime(Double timestamp) {

        return Instant.ofEpochMilli(timestamp.longValue()).atZone(ZoneOffset.UTC).format(DATE_FORMATTER);
    }

    private static String formatQuarter(long timestamp) {

        return Instant.ofEpochMilli(timestamp).atZone(ZoneOffset.UTC).format(QUARTER_FORMATTER);
    }

    private String formatYear(long timestamp) {

        return Instant.ofEpochMilli(timestamp).atZone(ZoneOffset.UTC).format(YEAR_FORMATTER);
    }

    private static String formatMonth(Long timestamp) {
        return Instant.ofEpochMilli(timestamp).atZone(ZoneOffset.UTC).format(MONTH_FORMATTER);
    }

    private static Long getNextMonthStep(long timestamp) {
        String current = formatMonth(timestamp);
        YearMonth ym = YearMonth.parse(current, MONTH_FORMATTER);
        ZonedDateTime zdt = ym.atDay(1).atStartOfDay().atZone(ZoneId.of("Z"));
        ZonedDateTime plusMonths = zdt.plusMonths(1);
        return plusMonths.toInstant().getEpochSecond() * 1000L - timestamp;
    }

    private static Long getNextQuarterStep(long timestamp) {
        String current = formatMonth(timestamp);
        YearMonth ym = YearMonth.parse(current, MONTH_FORMATTER);
        int quarter = ym.getMonthValue() / 3;
        ym = YearMonth.of(ym.getYear(), (quarter * 3) + 1);
        ZonedDateTime zdt = ym.atDay(1).atStartOfDay().atZone(ZoneId.of("Z"));
        ZonedDateTime plusMonths = zdt.plusMonths(3);
        return plusMonths.toInstant().getEpochSecond() * 1000L - timestamp;
    }

    private static Long getNextYearStep(long timestamp) {
        String current = formatMonth(timestamp);
        YearMonth ym = YearMonth.parse(current, MONTH_FORMATTER);
        ym = YearMonth.of(ym.getYear(), 1);
        ZonedDateTime zdt = ym.atDay(1).atStartOfDay().atZone(ZoneId.of("Z"));
        ZonedDateTime plusMonths = zdt.plusYears(1);
        return plusMonths.toInstant().getEpochSecond() * 1000L - timestamp;
    }

    private void generateFromTemplate(Replacer replacer, File path, String parent, String template, String filename) throws Exception {
        String fileContent = replacer.applyReplacements(getResourceContent(parent + "/" + template));
        StringBuffer newString = new StringBuffer();
        for(String line : fileContent.split("\r\n")) {
            String padRight = "";
            if(line.length() < LBL_LINE_CHARACTERS) {
                padRight = String.join("", Collections.nCopies(LBL_LINE_CHARACTERS - line.length(), " "));
            } else {
                LOGGER.warn(String.format("LBL length: %d \n'%s'", (line.length() + 2), line));
            }
            newString.append(line + padRight + "\r\n");
        }
        File newFile = new File(path, filename);
        try (PrintWriter out = new PrintWriter(newFile)) {
            out.write(newString.toString());
        }
    }
  

    private String getResourceContent(String resourcePath) throws Exception {
        return new String(Files.readAllBytes(Paths.get(getClass().getResource(resourcePath).toURI())));
    }
    
    private String getResourceFile(String resourcePath) throws Exception {
        return getClass().getResource(resourcePath).getFile();
    }
    

}
