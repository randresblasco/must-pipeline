package org.esa.must.pds;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FolderDefinition {

	private String name;
	private List<String> templates;
	private File folder;

	public FolderDefinition(String name) {
		this.name = name;
		this.templates = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setTemplates(List<String> templates) {
		this.templates = templates;
	}

	public List<String> getTemplates() {
		return templates;
	}

	public File getFolder() {
		return folder;
	}
	public void setFolder(File folder) {
		this.folder = folder;
	}
}
