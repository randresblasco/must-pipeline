package org.esa.must.pds;

import java.util.List;

public class Dataset {

    private String name;
    private String id;
    private List<ProductDefinition> products;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public List<ProductDefinition> getProducts() {
        return products;
    }
    public void setProducts(List<ProductDefinition> products) {
        this.products = products;
    }
    public String getHkSubsystem() {
        String[] id_fields = this.id.split("-");
        if (id_fields.length != 6) {
            throw new IllegalArgumentException("ID in YAML does not have the required format: RO-X-HK-3-xxxx-Vyyy");
        }
        return id_fields[4];
    }
    
    
    
    
}
