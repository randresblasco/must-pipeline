package org.esa.must.pds;

public class ProductDefinition {
    
    private String name;
    private String fileDuration;
    private String parameterId;
    private String thumbnail;

    public ProductDefinition() {
        // TODO Auto-generated constructor stub
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileDuration() {
        return fileDuration;
    }

    public void setFileDuration(String fileDuration) {
        this.fileDuration = fileDuration;
    }

    public String getParameterId() {
        return parameterId;
    }

    public void setParameterId(String parameterId) {
        this.parameterId = parameterId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean hasThumbnail() {
        return this.thumbnail != null && this.thumbnail.equalsIgnoreCase("YES");
    }
    
    
    

}
