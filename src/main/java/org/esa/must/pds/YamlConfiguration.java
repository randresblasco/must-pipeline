package org.esa.must.pds;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class YamlConfiguration {

		
	private List<Dataset> datasets;
	
	
	public YamlConfiguration() {
		
	}

	public static YamlConfiguration load(File datasets) {
	    

        try {
            Yaml yaml = new Yaml(new Constructor(YamlConfiguration.class));
            InputStream inputStream = new FileInputStream(datasets);
            YamlConfiguration configuration = yaml.load(inputStream);
            return configuration;
            
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	   	    
		return null;

	}

    public List<Dataset> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<Dataset> categories) {
        this.datasets = categories;
    }
	
	
}
