package org.esa.must.pds;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Replacer {
	
    private static final CharSequence LINESEP = "\r\n";

    private static final int MAXLENGTH = 78;
    private static final int INDENT = 34;
    
	Map<String, String> replacements = new HashMap<>();
	Map<String, List<String>> listReplacements = new HashMap<>();
	
	public void addReplacement(String key, String value) {
		replacements.put(key, value);
	}

	public void addListReplacement(String key, List<String> value) {
        listReplacements.put(key, value);
    }
	
	public String applyReplacements(String original) {
		
		String modified = new String(original);
		
		for(String key : replacements.keySet()) {
			modified = modified.replaceAll(String.format("\\{\\{%s\\}\\}", key), formatString(INDENT, MAXLENGTH, replacements.get(key)));
		}
		
		for(String key : listReplacements.keySet()) {
		    StringBuffer sb = new StringBuffer();
		    List<String> list = listReplacements.get(key);
		    for (int i = 0; i < list.size(); i++) {
		        String item = listReplacements.get(key).get(i);
		        if (i != 0) {
		            sb.append(String.join("", Collections.nCopies(INDENT, " ")));
		        }
		        sb.append(item);
		        if(i < list.size() - 1) {
		            sb.append("," + LINESEP);
		        }
		    }
		    modified = modified.replaceAll(String.format("\\{\\{%s\\}\\}", key), sb.toString());
        }
		return  modified;
	}
	
	
	public static String formatString(int indent, int maxLength, String line) {
	    
	    if ((line.length() + indent) < maxLength) {
            return line;
        }
        StringBuffer newString = new StringBuffer();
        
	    try {
        
        int counter = indent;
        int lineIndex = 0;
        int lastWord = -1;
        StringBuffer lineProcessed =  new StringBuffer();
        int total = 0;
        while(lineIndex < line.length()) {
            if (counter == maxLength) {
                String indentStr = (total == 0) ? "" : String.join("", Collections.nCopies(indent, " ")); 
                if (lastWord != -1) {
                    newString.append(indentStr + lineProcessed.subSequence(0, lastWord -1 ).toString() + LINESEP);
                    total += lastWord;
                    lineIndex = total;
                } else {
                    newString.append(indentStr + lineProcessed.toString() + LINESEP);
                    total+= lineProcessed.length();
                }
                counter = indent;
                lastWord = -1;
                lineProcessed =  new StringBuffer();
            }
            char charAt = line.charAt(lineIndex);
            lineProcessed.append(charAt);
            
            if (charAt == ' ') {
                lastWord = -1;
            } else {
                lastWord = (lastWord == -1) ? lineProcessed.length() - 1 : lastWord;
            }
            
            counter ++;
            lineIndex ++;
        }
        if (lineProcessed.length() > 0) {
            newString.append(String.join("", Collections.nCopies(indent, " ")) + lineProcessed.toString());
        }
        
	    } catch (Exception e) {
	        e.printStackTrace();
	        throw new IllegalArgumentException("Problems formatting " + line);
            
        }
        return newString.toString();
    }
}
