package org.esa.must.pds;

import java.io.InputStream;
import java.util.Properties;

public class Configuration {

	private static final String SOFTWARE_VERSION_KEY = "software.version";
	private static final String BUILD_TIMESTAMP_KEY = "build.timestamp";
    private static Configuration singleton = null;
	private static InputStream propFile = Configuration.class.getResourceAsStream("/configuration.properties");
		
	private String softwareVersion;
	private String buildTimestamp;


	public synchronized static Configuration getInstance() {
		if (singleton == null) {
			singleton = new Configuration();
		}
		return singleton;
	}
	
	
	private Configuration() {
		load();
	}

	private void load() {
		Properties prop = new Properties();
		try {
			prop.load(propFile);
			softwareVersion = prop.getProperty(SOFTWARE_VERSION_KEY, "N/A");
			buildTimestamp = prop.getProperty(BUILD_TIMESTAMP_KEY, "N/A");
			
			
		} catch (Exception e) {
		    e.printStackTrace();
		    throw new RuntimeException("Cannot load the configuration.prop file");
			
		}

	}
	
	
	public String getSoftwareVersion() {
        return softwareVersion;
    }
	
	public String getBuildTimestamp() {
        return buildTimestamp;
    }
}
