# README #

The must-pipeline is a command line tool that generates the PDS3 DATA section containing the time series of values of the MUST telemetry parameters passed as argument.    
The tool, writen in java:

* connects to the MUST database
* extracts the telemetry data associated to the parameters identified as input
* formats and writes in PDS compliant products
* creates the label files with the metadata associated


### How do I get set up? ###

* Must DB connection


### Tool execution ###

```
./must-pipeline
```

### Who do I talk to? ###

* r.andres.blasco@gmail.com

### List of current parameters and its type in must###

```
+------+----------+-----------+
| PID  | PNAME    | dbtype    |
+------+----------+-----------+
|   81 | NACP0435 | uTinyInt  |
| 7881 | NACP1300 | uInt      |
| 7882 | NACP1301 | uInt      |
| 7883 | NACP1800 | uInt      |
| 7884 | NACP1801 | uInt      |
| 7885 | NACP2300 | uInt      |
| 7886 | NACP2301 | uInt      |
| 7887 | NACP2800 | uInt      |
| 7888 | NACP2801 | uInt      |
| 3140 | NACW0D0A | uSmallInt |
| 3146 | NACW0E00 | float     |
| 3147 | NACW0E01 | float     |
| 3148 | NACW0E02 | float     |
| 3155 | NACW0E0A | float     |
| 3156 | NACW0E0B | float     |
| 3157 | NACW0E0C | float     |
| 3164 | NACW0E0K | float     |
| 3165 | NACW0E0L | float     |
| 3166 | NACW0E0M | float     |
| 3324 | NACW0M0J | float     |
| 3325 | NACW0M0K | float     |
| 3326 | NACW0M0L | float     |
| 3327 | NACW0M0M | float     |
| 3328 | NACW0M0N | float     |
| 3329 | NACW0M0O | float     |
| 3330 | NACW0M0P | float     |
| 3331 | NACW0M0Q | float     |
| 3332 | NACW0M0R | float     |
| 3333 | NACW0M0S | float     |
| 3334 | NACW0M0T | float     |
| 3335 | NACW0M0U | float     |
| 3336 | NACW0M0V | float     |
| 3337 | NACW0M0W | float     |
| 3338 | NACW0M0X | float     |
| 3339 | NACW0M0Y | float     |
| 3340 | NACW0M0Z | float     |
| 3341 | NACW0M10 | float     |
| 3342 | NACW0M11 | float     |
| 3343 | NACW0M12 | float     |
| 3344 | NACW0M13 | float     |
| 3345 | NACW0M14 | float     |
| 3346 | NACW0M15 | float     |
| 3347 | NACW0M16 | float     |
| 3381 | NACW0P0B | float     |
| 3522 | NACW120A | float     |
| 3523 | NACW120B | float     |
| 3538 | NACW120U | float     |
| 3539 | NACW120V | float     |
| 3547 | NACW1217 | float     |
| 3548 | NACW1218 | float     |
| 3552 | NACW121C | float     |
| 3553 | NACW121D | float     |
| 3701 | NACW1D05 | float     |
| 3703 | NACW1D07 | float     |
| 3704 | NACW1D09 | float     |
| 3705 | NACW1D0A | float     |
| 3709 | NACW1D0E | float     |
| 3710 | NACW1D0O | float     |
| 3711 | NACW1D0P | float     |
| 3712 | NACW1D0Q | float     |
| 3734 | NACW1G02 | float     |
| 3749 | NACW1K0H | uSmallInt |
| 3832 | NACW1K2X | uSmallInt |
| 3928 | NACW1L0P | float     |
| 3929 | NACW1L0Q | float     |
| 3950 | NACW1L1I | float     |
| 3951 | NACW1L1J | float     |
| 7190 | NACW1R0Q | float     |
| 7191 | NACW1R0R | float     |
| 7250 | NACW1R1K | float     |
| 7251 | NACW1R1L | float     |
| 4013 | NACX0001 | float     |
| 4014 | NACX0002 | float     |
| 4015 | NACX0003 | float     |
| 4020 | NACX0008 | float     |
| 4021 | NACX0009 | float     |
| 4022 | NACX0010 | float     |
| 4023 | NACX0011 | float     |
| 4072 | NACX0502 | uSmallInt |
| 7320 | NACX0503 | uSmallInt |
| 5294 | NAWD0V05 | uTinyInt  |
| 9301 | NAWG0060 | float     |
| 5641 | NDMW0D0A | uSmallInt |
| 5732 | NDMWO20L | uSmallInt |
| 6898 | NTTD1020 | uTinyInt  |
| 6900 | NTTD1027 | uTinyInt  |
| 6903 | NTTD102A | uTinyInt  |
| 6909 | NTTD1069 | uTinyInt  |
| 6910 | NTTD106A | uTinyInt  |
| 6920 | NTTD2020 | uTinyInt  |
| 6922 | NTTD2027 | uTinyInt  |
| 6925 | NTTD202A | uTinyInt  |
| 6931 | NTTD2069 | uTinyInt  |
| 6932 | NTTD206A | uTinyInt  |
| 6966 | NTTX0023 | uTinyInt  |
| 6973 | NTTX4011 | uTinyInt  |
| 6975 | NTTX4023 | uTinyInt  |
| 6982 | NTTX5011 | uTinyInt  |
| 6984 | NTTX5023 | uTinyInt  |
| 7890 | NAAD6011 | uTinyInt  |
| 7891 | NAAD6021 | uTinyInt  |
| 7892 | NAAD6031 | uTinyInt  |
| 7893 | NAAD6041 | uTinyInt  |
| 9329 | NAAG0005 | float     |
| 9330 | NAAG0006 | float     |
| 9331 | NAAG0007 | float     |
| 9332 | NAAG0008 | float     |
| 9315 | NACG0010 | float     |
| 9316 | NACG0011 | float     |
| 9317 | NACG0012 | float     |
| 9318 | NACG0013 | float     |
| 9319 | NACG0014 | float     |
| 9320 | NACG0015 | float     |
| 9321 | NACG0016 | float     |
| 9322 | NACG0017 | float     |
| 3226 | NACW0G05 | float     |
| 3237 | NACW0G0H | float     |
| 3248 | NACW0G0T | float     |
| 3259 | NACW0G15 | float     |
| 3564 | NACW1306 | float     |
| 3565 | NACW1307 | float     |
| 3739 | NACW1K05 | uSmallInt |
| 3740 | NACW1K06 | float     |
| 3745 | NACW1K0D | float     |
| 3746 | NACW1K0E | float     |
| 3747 | NACW1K0F | float     |
| 3750 | NACW1K0I | uSmallInt |
| 3751 | NACW1K0J | uSmallInt |
| 3752 | NACW1K0K | uSmallInt |
| 3753 | NACW1K0L | float     |
| 3754 | NACW1K0M | float     |
| 3756 | NACW1K0P | float     |
| 3757 | NACW1K0Q | float     |
| 3758 | NACW1K0R | float     |
| 3759 | NACW1K0S | float     |
| 3760 | NACW1K0T | float     |
| 3761 | NACW1K0U | float     |
| 3762 | NACW1K0V | float     |
| 3763 | NACW1K0W | float     |
| 3764 | NACW1K0X | float     |
| 3765 | NACW1K0Y | float     |
| 3766 | NACW1K0Z | float     |
| 3767 | NACW1K10 | float     |
| 3768 | NACW1K11 | float     |
| 3769 | NACW1K12 | float     |
| 3770 | NACW1K13 | float     |
| 3771 | NACW1K14 | float     |
| 3772 | NACW1K15 | float     |
| 3773 | NACW1K16 | float     |
| 3774 | NACW1K17 | float     |
| 3775 | NACW1K18 | float     |
| 3776 | NACW1K19 | float     |
| 3777 | NACW1K1A | float     |
| 3778 | NACW1K1B | float     |
| 3779 | NACW1K1C | float     |
| 3780 | NACW1K1D | float     |
| 3781 | NACW1K1E | float     |
| 3782 | NACW1K1F | float     |
| 3783 | NACW1K1G | float     |
| 3784 | NACW1K1H | float     |
| 3785 | NACW1K1I | float     |
| 3786 | NACW1K1J | float     |
| 3787 | NACW1K1K | float     |
| 3788 | NACW1K1L | float     |
| 3789 | NACW1K1M | float     |
| 3790 | NACW1K1N | float     |
| 3791 | NACW1K1O | float     |
| 3792 | NACW1K1P | float     |
| 3793 | NACW1K1Q | float     |
| 3794 | NACW1K1R | float     |
| 3795 | NACW1K1S | float     |
| 3796 | NACW1K1T | float     |
| 3797 | NACW1K1U | float     |
| 3798 | NACW1K1V | float     |
| 3799 | NACW1K1W | float     |
| 3800 | NACW1K1X | float     |
| 3801 | NACW1K1Y | float     |
| 3802 | NACW1K1Z | float     |
| 3803 | NACW1K20 | float     |
| 3804 | NACW1K21 | float     |
| 3805 | NACW1K22 | float     |
| 3806 | NACW1K23 | float     |
| 3807 | NACW1K24 | float     |
| 3808 | NACW1K25 | float     |
| 3809 | NACW1K26 | float     |
| 3810 | NACW1K27 | float     |
| 3811 | NACW1K28 | float     |
| 3812 | NACW1K29 | float     |
| 3813 | NACW1K2A | float     |
| 3814 | NACW1K2B | float     |
| 3815 | NACW1K2C | float     |
| 3816 | NACW1K2D | float     |
| 3817 | NACW1K2E | float     |
| 3818 | NACW1K2F | float     |
| 3822 | NACW1K2L | uSmallInt |
| 3823 | NACW1K2M | float     |
| 3828 | NACW1K2T | float     |
| 3829 | NACW1K2U | float     |
| 3830 | NACW1K2V | float     |
| 3833 | NACW1K2Y | uSmallInt |
| 3834 | NACW1K2Z | uSmallInt |
| 3835 | NACW1K30 | uSmallInt |
| 3836 | NACW1K31 | float     |
| 3837 | NACW1K32 | float     |
| 3839 | NACW1K35 | float     |
| 3840 | NACW1K36 | float     |
| 3841 | NACW1K37 | float     |
| 3842 | NACW1K38 | float     |
| 3843 | NACW1K39 | float     |
| 3844 | NACW1K3A | float     |
| 3845 | NACW1K3B | float     |
| 3846 | NACW1K3C | float     |
| 3847 | NACW1K3D | float     |
| 3848 | NACW1K3E | float     |
| 3849 | NACW1K3F | float     |
| 3850 | NACW1K3G | float     |
| 3851 | NACW1K3H | float     |
| 3852 | NACW1K3I | float     |
| 3853 | NACW1K3J | float     |
| 3854 | NACW1K3K | float     |
| 3855 | NACW1K3L | float     |
| 3856 | NACW1K3M | float     |
| 3857 | NACW1K3N | float     |
| 3858 | NACW1K3O | float     |
| 3859 | NACW1K3P | float     |
| 3860 | NACW1K3Q | float     |
| 3861 | NACW1K3R | float     |
| 3862 | NACW1K3S | float     |
| 3863 | NACW1K3T | float     |
| 3864 | NACW1K3U | float     |
| 3865 | NACW1K3V | float     |
| 3866 | NACW1K3W | float     |
| 3867 | NACW1K3X | float     |
| 3868 | NACW1K3Y | float     |
| 3869 | NACW1K3Z | float     |
| 3870 | NACW1K40 | float     |
| 3871 | NACW1K41 | float     |
| 3872 | NACW1K42 | float     |
| 3873 | NACW1K43 | float     |
| 3874 | NACW1K44 | float     |
| 3875 | NACW1K45 | float     |
| 3876 | NACW1K46 | float     |
| 3877 | NACW1K47 | float     |
| 3878 | NACW1K48 | float     |
| 3879 | NACW1K49 | float     |
| 3880 | NACW1K4A | float     |
| 3881 | NACW1K4B | float     |
| 3882 | NACW1K4C | float     |
| 3883 | NACW1K4D | float     |
| 3884 | NACW1K4E | float     |
| 3885 | NACW1K4F | float     |
| 3886 | NACW1K4G | float     |
| 3887 | NACW1K4H | float     |
| 3888 | NACW1K4I | float     |
| 3889 | NACW1K4J | float     |
| 3890 | NACW1K4K | float     |
| 3891 | NACW1K4L | float     |
| 3892 | NACW1K4M | float     |
| 3893 | NACW1K4N | float     |
| 3894 | NACW1K4O | float     |
| 3895 | NACW1K4P | float     |
| 3896 | NACW1K4Q | float     |
| 3897 | NACW1K4R | float     |
| 3898 | NACW1K4S | float     |
| 3899 | NACW1K4T | float     |
| 3900 | NACW1K4U | float     |
| 3901 | NACW1K4V | float     |
| 4028 | NACX0016 | float     |
| 4029 | NACX0017 | float     |
| 4032 | NACX0020 | float     |
| 4033 | NACX0021 | float     |
| 4034 | NACX0022 | float     |
| 4071 | NACX0500 | uSmallInt |
| 7319 | NACX0501 | uSmallInt |
| 5362 | NAWD1K04 | uTinyInt  |
| 5366 | NAWD1K0A | uTinyInt  |
| 9286 | NAWG0025 | float     |
| 9287 | NAWG0026 | float     |
| 6076 | NPWD1024 | float     |
| 6077 | NPWD102B | float     |
| 6080 | NPWD1044 | float     |
| 6081 | NPWD104B | float     |
| 6089 | NPWD1104 | float     |
| 6151 | NPWD1704 | float     |
| 6251 | NPWD2260 | float     |
| 6252 | NPWD2268 | float     |
| 6310 | NPWD2920 | float     |
| 6311 | NPWD2928 | float     |
| 6955 | NTTDX101 | uTinyInt  |
| 6956 | NTTDX102 | uTinyInt  |
| 6958 | NTTDX201 | uTinyInt  |
| 6959 | NTTDX202 | uTinyInt  |
| 6965 | NTTX0022 | uTinyInt  |
+------+----------+-----------+

```
### MUST Tables ###
```
utinyintparamvalues

+----------+------------------------+------+-----+---------+-------+
| Field    | Type                   | Null | Key | Default | Extra |
+----------+------------------------+------+-----+---------+-------+
| PID      | smallint(9) unsigned   | NO   | PRI | 0       |       |
| datetime | decimal(13,0) unsigned | NO   | PRI | 0       |       |
| value    | tinyint(3) unsigned    | NO   |     | 0       |       |
+----------+------------------------+------+-----+---------+-------+

usmallintparamvalues
+----------+------------------------+------+-----+---------+-------+
| Field    | Type                   | Null | Key | Default | Extra |
+----------+------------------------+------+-----+---------+-------+
| PID      | smallint(9) unsigned   | NO   | PRI | 0       |       |
| datetime | decimal(13,0) unsigned | NO   | PRI | 0       |       |
| value    | smallint(5) unsigned   | NO   |     | 0       |       |
+----------+------------------------+------+-----+---------+-------+

floatparamvalues
+----------+------------------------+------+-----+---------+-------+
| Field    | Type                   | Null | Key | Default | Extra |
+----------+------------------------+------+-----+---------+-------+
| PID      | smallint(9) unsigned   | NO   | PRI | 0       |       |
| datetime | decimal(13,0) unsigned | NO   | PRI | 0       |       |
| value    | float                  | NO   |     | 0       |       |
+----------+------------------------+------+-----+---------+-------+
```


### Used MUST/MySQL Types###

```
| TINYINT[Length]  | 1 byte  | Range of -128 to 127 or 0 to 255 unsigned         |
| SMALLINT[Length] | 2 bytes | Range of -32,768 to 32,767 or 0 to 65535 unsigned |
| FLOAT            | 4 bytes | A small number with a floating decimal point      |
```
